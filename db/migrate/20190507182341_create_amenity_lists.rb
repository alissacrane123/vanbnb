class CreateAmenityLists < ActiveRecord::Migration[5.2]
  def change
    create_table :amenity_lists do |t|
      t.integer :listing_id, null: false
      t.string :color
      t.string :brand
      t.string :condition
      t.integer :guest_num
      t.boolean :air_con
      t.boolean :heating
      t.boolean :tv
      t.boolean :kitchen

      t.timestamps
    end
    add_index :amenity_lists, :listing_id
  end
end
