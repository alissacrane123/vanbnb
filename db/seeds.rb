
require 'open-uri'
Booking.destroy_all
AmenityList.destroy_all
Listing.destroy_all
User.destroy_all

# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

user1 = User.create!(email: "alissa@gmail.com", password: "password", fname: "Alissa", lname: "crane")
user2 = User.create!(email: "brandon@gmail.com", password: "password", fname: "Brandon", lname: "crane")
user3 = User.create!(email: "bodhi@gmail.com", password: "password", fname: "Bodhi", lname: "daniels")
user4 = User.create!(email: "pancake@gmail.com", password: "password", fname: "Pancake", lname: "smith")

big = Listing.create!( owner_id: user1.id, name: 'Big Bertha', description: "The newest and most fuel and space efficient of all of our models, the Luxury VW was designed with spontaneity, freedom, and ease in mind – allowing you to chase the surf breaks, take detours, and find hidden gems. This latest model is perfect for couples or families who need the extra space that many other vans can’t offer. Get ready to zip around town, navigate tight turns, and get off the beaten track – without sacrificing any comforts or functionality.", 
                        price: 100.00, address: '1570 31st ave', city: 'san francisco', state: 'ca', zipcode: 94122, lat: 37.757489, lng: -122.48993 )

        bigPhotos = ["https://s3-us-west-1.amazonaws.com/vanbnb-seeds/big1.jpg", "https://s3-us-west-1.amazonaws.com/vanbnb-seeds/big2.jpg", "https://s3-us-west-1.amazonaws.com/vanbnb-seeds/big3.jpg", "https://s3-us-west-1.amazonaws.com/vanbnb-seeds/big4.jpg"]

        bigPhotos.each_with_index do |photo_url, i|
            file = open(photo_url)
            big.photos.attach(io: file, filename: "big_van#{i}.jpeg")
        end

        AmenityList.create!(listing_id: big.id, color: "white", brand: "Volkswagen", condition: "excellent", guest_num: 8, 
                              air_con: true, heating: true, tv: true, kitchen: true)


green = Listing.create!( owner_id: user2.id, name: 'The Green Machine', description: "This big beauty is probably the best built and most comfortable van on the road. The comfort and space and most importantly, the quality, was a priority on the custom build! Perfect for families, this van allows you to take as long of a trip as you want without having to sacrifice the comforts of home. From the moment you drive away to the time you drop off, you'll experience the freedom that only a classic road trip can offer.", 
                        price: 95.00, address: '601 19th ave',  city: 'san francisco', state: 'ca', zipcode: 94121, lat: 37.776286, lng: -122.477659)

        greenPhotos = ["https://s3-us-west-1.amazonaws.com/vanbnb-seeds/green1.jpeg", "https://s3-us-west-1.amazonaws.com/vanbnb-seeds/green2.jpeg", "https://s3-us-west-1.amazonaws.com/vanbnb-seeds/green3.jpeg", "https://s3-us-west-1.amazonaws.com/vanbnb-seeds/green4.jpeg"]

        greenPhotos.each_with_index do |photo_url, i|
          file = open(photo_url)
          green.photos.attach(io: file, filename: "green_van#{i}.jpeg")
        end

        AmenityList.create!(listing_id: green.id, color: "green", brand: "Clark", condition: "excellent", guest_num: 8, 
                            air_con: true, heating: true, tv: false, kitchen: true)



vw = Listing.create!( owner_id: user3.id, name: 'RoadRunner', description: "There is no better vacation than exploring the California coast in a VW camper van (or beyond, if you have the time and the van!). From the moment you drive away to the time you drop off, you'll experience the freedom that only a classic road trip can offer. No tour guides, no restrictions, no boundaries. Just you and the van and the road. This classic is equipped with everything you need to have a comfortable trip.", 
                        price: 100.00, address: '3049 23rd st', city: 'san francisco', state: 'ca', zipcode: 94110, lat: 37.754069, lng: -122.41246 )

        vwPhotos = ["https://s3-us-west-1.amazonaws.com/vanbnb-seeds/vw1.jpg", "https://s3-us-west-1.amazonaws.com/vanbnb-seeds/vw2.jpg", "https://s3-us-west-1.amazonaws.com/vanbnb-seeds/vw3.jpg", "https://s3-us-west-1.amazonaws.com/vanbnb-seeds/vw4.jpeg"]

        vwPhotos.each_with_index do |photo_url, i|
          file = open(photo_url)
          vw.photos.attach(io: file, filename: "vw_van#{i}.jpg")
        end

        AmenityList.create!(listing_id: vw.id, color: "red", brand: "Volkswagen", condition: "excellent", guest_num: 5, 
                            air_con: true, heating: true, tv: false, kitchen: true)
     




lux = Listing.create!( owner_id: user4.id, name: 'Luxury Cruiser', description: "A perfect fit for prestige, luxury, and power, the Mercedes Sprinter Van enables the travelers to experience the pleasure and extra comfort at its top. Relaxing couches make the passengers feel amazingly comfy during a genuinely comfortable and instantaneous ride to their place. With the Mercedes, a journey is not just an ordinary ride but a fabulous traveling experience. This van is the most reliable van on the road and is perfect for a trip with family or friends. This is an easily driven vehicle that makes the journey highly comfortable no matter whether you are on a flat road or bumpy path.", 
                        price: 100.00, address: '1945 jefferson st', city: 'san francisco', state: 'ca', zipcode: 94123, lat: 37.804196, lng: -122.44437 )
 
        luxPhotos = ["https://s3-us-west-1.amazonaws.com/vanbnb-seeds/lux1.jpg", "https://s3-us-west-1.amazonaws.com/vanbnb-seeds/lux2.jpg", "https://s3-us-west-1.amazonaws.com/vanbnb-seeds/lux3.jpg", "https://s3-us-west-1.amazonaws.com/vanbnb-seeds/lux4.jpg"]

        luxPhotos.each_with_index do |photo_url, i|
          file = open(photo_url)
          lux.photos.attach(io: file, filename: "lux_van#{i}.jpg")
        end
                        
      AmenityList.create!(listing_id: lux.id, color: "white", brand: "Mercedes", condition: "excellent", guest_num: 8, 
                          air_con: true, heating: true, tv: true, kitchen: true)


# Listing.create!( owner_id: 6, name: 'Escape Van', description: 'Reliable and fast.', price: 100.00, address: 'alcatraz island',
#                 city: 'san francisco', state: 'ca', zipcode: 94113, lat: 37.826160, lng: -122.42188 )
      
#       AmenityList.create!(listing_id: green.id, color: "green", brand: "Clark", condition: "excellent", guest_num: 8, 
#                           air_con: true, heating: true, tv: false, kitchen: true)

# Listing.create!( owner_id: 4, name: 'Speed Demon', description: 'This van will get you where you need to go fast!', price: 100.00, address: '700 state street',
#                 city: 'santa barbara', state: 'ca', zipcode: 93101, lat: 34.419064, lng: -119.69804 )
      
#       AmenityList.create!(listing_id: green.id, color: "green", brand: "Clark", condition: "excellent", guest_num: 8, 
#                           air_con: true, heating: true, tv: false, kitchen: true)



# Listing.create!(
#   owner_id: 2,
#   name: 'Big Betsy',
#   description: 'This ol gal is ready to take you and your fam on the trip of a lifetime!',
#   price: 90.00,
#   address: '601 19th ave',
#   city: 'san francisco',
#   state: 'ca',
#   zipcode: 94121,
#   lat: 37.776286,
#   lng: -122.477659
# )