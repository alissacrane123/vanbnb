Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html

  root "static_pages#root"

  namespace :api, defaults: {format: :json} do
    resources :listings, only: [:create, :index, :show ]
    resources :amenity_lists, only: [:index, :show ]

    resources :bookings, only: [:index, :show, :create, :update, :destroy]
    resources :users, only: [:create]
    resources :reviews, only: [:create, :show, :index]
    resource :session, only: [:create, :destroy]
  end

end
