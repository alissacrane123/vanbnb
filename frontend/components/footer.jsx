import React from 'react';

const Footer = (props) => (
  <div className="footer-container">
    <div className="footer-container2">
      <a href="https://www.linkedin.com/in/alissa-crane-92a554122" target="_blank" className="personal">
        LinkedIn 
      </a>
    {/* <div className="personal">LinkedIn</div> */}
    <div className="personal">·</div>
    <div className="personal">GitHub</div>
    <div className="personal">·</div>
    <div className="personal">Portfolio</div>
  </div>
  </div>
)

export default Footer 