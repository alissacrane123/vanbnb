import React from 'react';
import { Link } from 'react-router-dom';
import { withRouter } from 'react-router-dom';

class BookingIndexItem extends React.Component {
  constructor(props) {
    super(props);
    this.state = { listing: null };
    this.handleClick = this.handleClick.bind(this);
  }

  componentDidMount() {
    let { booking, listings } = this.props
    let listing = listings[booking.listing_id];
    this.setState({ listing: listing })
  }

  handleClick(e) {
    if (this.state.listing) {
      const listingId = this.state.listing.id;
      this.props.history.push(`/listings/${listingId}`)
    }
  }

  formatDate() {
    let { booking, past } = this.props;
    let months = ['JAN', 'FEB', 'MARCH', 'APRIL', 'MAY', 'JUNE',
                  'JULY', 'AUG', 'SEPT', 'OCT', 'NOV', 'DEC']

    let month1 = months[Number(booking.check_in.slice(5, 7)) - 1]
    let day1 = Number(booking.check_in.slice(8, 10))
    let yr1 = Number(booking.check_in.slice(0, 4))

    let month2 = months[Number(booking.check_out.slice(5, 7)) - 1]
    let day2 = Number(booking.check_out.slice(8, 10))
    let yr2 = Number(booking.check_out.slice(0, 4))

    if (past) {
      return `${month1} ${yr1}`
    } else {
      return `${month1} ${day1} - ${month2} ${day2}`
    }
  }

  render() {
    let { booking, listings, past } = this.props    
    if (!booking ) return null;
    let listing = listings[booking.listing_id]
    if (!listing) return null;

    let photoUrl = listing.photoUrls[0];

    let date = this.formatDate();

    let reviewLink;
    if (past) {
      reviewLink = <div className="booking-review-container"><Link to={`/review/${listing.id}`}>Write a review</Link></div>;
    } else {
      reviewLink = <div></div>
    }


    return (
      <div value={listing.id} className="booking-container">
        <div className="booking-photo-container" onClick={this.handleClick}>
          <img src={photoUrl} className="booking-photo" />
        </div>
        <div className="booking-info-container">
          <div className="booking-info">
            <div className="booking-date">{date}</div>
            <div className="booking-name">{listing.name}</div>
          </div>         
          { reviewLink }
        </div>
      </div>
    )
  }
}

export default withRouter(BookingIndexItem);