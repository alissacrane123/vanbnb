import React from 'react';
import BookingIndexItem from './booking_item';

class BookingIndex extends React.Component {
  constructor(props) {
    super(props);
  }

  componentDidMount() {
    this.props.fetchBookings();
  }

  upcoming2(booking) {
    let today = new Date();
    let todayDate = Date.parse(today);
    let bookingDate = Date.parse(booking.check_in);

    if (todayDate < bookingDate) return true 
  }
  
  render() {
    let { bookings, listings } = this.props;

    // if (bookings === undefined || bookings.length == 0 ) return null;
    if (bookings === undefined  ) return null;

    let upcoming = [];
    let past = [];

    for (let i = 0; i < bookings.length; i++) {
      let booking = bookings[i];
      if (this.upcoming2(booking)) { 
        upcoming.push(<BookingIndexItem booking={booking} listings={listings} past={false} key={booking.id} />);
      } else {
        past.push(<BookingIndexItem booking={booking} listings={listings} past={true} key={booking.id} />);
      }   
    }

    return (
      <div className="bookings-container">
        <div className="upcoming-container">
          <div className="BI-header-container">
            <div className="BI-header">
              Upcoming Trips
            </div>
          </div>
          <div className="upcoming-bookings-container">
            { upcoming }
          </div>
        </div>

        <div className="past-container">
          <div className="BI-header-container">
            <div className="BI-header">
              Past Trips
            </div>
          </div>
          <div className="past-bookings-container">
            { past}
          </div>
        </div>
      </div>
    )
  }
  
}

export default BookingIndex;