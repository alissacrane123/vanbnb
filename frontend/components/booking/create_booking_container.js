
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import {  createBooking  } from '../../actions/booking_actions';
import { openModal, closeModal } from '../../actions/modal_actions';
import BookingForm from './booking_form';



const mapStateToProps = (state, ownProps) => {

  const listing = Object.values(state.entities.listings)[0];
  const listingId = Object.keys(state.entities.listings);

  return {
    errors: state.errors.session.errors,
    formType: 'booking',
    currentUserId: state.session.id,
    listingId,
    listing,
    booking: { checkIn: null, checkOut: null, guestNum: null}

    // switchLink: <Link to="/signup">Sign up</Link>
  };
}

const mapDispatchToProps = dispatch => {
  return {
    openModal: modal => dispatch(openModal(modal)),
    closeModal: () => dispatch(closeModal()),
    action: booking => dispatch(createBooking(booking))
  };
}

const CreateBookingContainer = connect(mapStateToProps, mapDispatchToProps)(BookingForm);

export default withRouter(CreateBookingContainer);