import { connect } from 'react-redux';
import React from 'react';
import { withRouter } from 'react-router-dom';
import { fetchBooking, createBooking, fetchBookings } from '../../actions/booking_actions';
import { fetchListings } from '../../actions/listing_actions';
import BookingIndex from './booking_index';

const mapStateToProps = (state, ownProps) => {
  // debugger
  return {
    currentUser: state.session,
    bookings: Object.values(state.entities.bookings),
    listings: state.entities.listings
  }
}

const mapDispatchToProps = dispatch => {
  return {
    fetchBookings: () => dispatch(fetchBookings()),
    fetchListings: () => dispatch(fetchListings())
  }
}

const bookingIndexContainer = connect(mapStateToProps, mapDispatchToProps)(BookingIndex);
export default withRouter(bookingIndexContainer);