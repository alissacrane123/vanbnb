import React from 'react';
// import { Link } from 'react-router-dom';
import { withRouter } from 'react-router-dom';

class ReviewItem extends React.Component {

  componentDidMount() {
    let { review, fetchListing } = this.props;
    let listingId = review.listing_id;

    fetchListing(listingId);
  }
  
  render() {
    let { listing, review } = this.props;

    if (listing.length === 0) return null;
    
    return(
      <div className="review-item-container">
        <div>{review.body}</div>

      </div>
    )
  }
}

export default ReviewItem;