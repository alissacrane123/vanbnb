import { connect } from 'react-redux';
import React from 'react';
import { withRouter } from 'react-router-dom';
import { fetchReviews } from '../../actions/review_actions';
import { fetchListing } from '../../actions/listing_actions';
import { fetchBookings } from '../../actions/booking_actions';
import ReviewIndex from './review_index';

const mapStateToProps = (state, ownProps) => {
  // const listingId = parseInt(ownProps.match.params.listingId)
  return {
    currentUser: state.session,
    reviews: Object.values(state.entities.reviews),
    listings: state.entities.listings,
    listing: Object.values(state.entities.listings),
    bookings: Object.values(state.entities.bookings)
    // listingId
  }
}

const mapDispatchToProps = dispatch => {
  return {
    // fetchReviews: () => dispatch(fetchReviews()),
    fetchListing: (id) => dispatch(fetchListing(id)),
    fetchListings: () => dispatch(fetchListings()),
    fetchBookings: () => dispatch(fetchBookings())
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(ReviewIndex);