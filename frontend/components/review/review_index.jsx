import React from 'react';
import ReviewItem from './review_item';

class ReviewIndex extends React.Component {
  constructor(props) {
    super(props);
  }

  componentDidMount() {
    // this.props.fetchBookings();

    // this.props.fetchListings();
    // this.props.fetchReviews();
  }


  render() {
    let { reviews, listing, fetchListing } = this.props;

    if (reviews.length === 0) return null;

    reviews = reviews.map(review => (
      <div key={review.id} className="review-container">
        <div>{review.body}</div>
      </div>
    ))

    return (
      <div className="review-index-container">
        {reviews}
        
      </div>
    )
  }
}

export default ReviewIndex;