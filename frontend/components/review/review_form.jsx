import React from  'react';
import { withRouter } from 'react-router-dom';

class ReviewForm extends React.Component {
  constructor(props) {
    super(props);

    this.state = { body: '', listing_id: this.props.listingId, rating: 0  }

    this.handleChange =  this.handleChange.bind(this);
    this.handleClick = this.handleClick.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleChange(e) {
    this.setState({ body: e.currentTarget.value })
  }

  handleClick(e) {
    this.setState({ rating:  e.currentTarget.value })
  }

  handleSubmit(e) {
    e.preventDefault();
    let review = this.state;
    
    this.props.createReview(review);
    this.props.history.push(`/listings/${this.props.listingId}`);
  }


  render() {

    return (
      <div className="RF-container">
        <div className="RF-header-container">
          <div className="RF-header">Submit a new review below</div>
        </div>
        
        <div className="RF-body-container">
          {/* <div className="RF-body-header-container">
            <div className="RF-body-header">Describe Your Experience</div>
          </div> */}
          <div className="RF-body-input-container">
            <textarea className="RF-body-input" onChange={this.handleChange} placeholder="Describe your experience..."/>
          </div>
        </div>

        <div className="RF-rating-container">
          <div className="RF-rating-header-container">
            <div className="RF-rating-header">Rate Your Experience</div>
          </div>
          <div className="RF-ratings-input-container">
            <input className="RF-ratings-input" onClick={this.handleClick} type="radio" id="star" value="1"/>
            <input className="RF-ratings-input" onClick={this.handleClick} type="radio" id="star" value="2"/>
            <input className="RF-ratings-input" onClick={this.handleClick} type="radio" id="star" value="3"/>
            <input className="RF-ratings-input" onClick={this.handleClick} type="radio" id="star" value="4"/>
            <input className="RF-ratings-input" onClick={this.handleClick} type="radio" id="star" value="5"/>
          </div>
        </div>

        <div className="review-submit-container">
          <button onClick={this.handleSubmit} className="review-submit">Submit</button>
        </div>

      </div>
    )
  }
}

export default withRouter(ReviewForm);