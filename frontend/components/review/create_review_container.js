
import { connect } from 'react-redux';
import React from 'react';
import { withRouter } from 'react-router-dom';
import { createReview } from '../../actions/review_actions';
import ReviewForm from './review_form';

const mapStateToProps = (state, ownProps) => {
  const listingId = parseInt(ownProps.match.params.listingId)
  return {
    currentUser: state.session,
    listingId
  }
}

const mapDispatchToProps = dispatch => {
  return {
    createReview: review => dispatch(createReview(review)),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(ReviewForm);