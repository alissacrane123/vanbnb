import React from 'react';
import { Link } from 'react-router-dom';
import NavBarContainer from '../navbar/navbar2_container';
import SearchFormContainer from '../search/search_form_container';
import Footer from '../footer';

class Splash extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    const guests = [1, 2, 3, 4, 5, 6, 7, 8, 9 ,10].map(num => (
      <option className="splash-guests" placeholder="Guests">{num}</option>
    ))

    return (
      <div className="splash">
        
        <div className="splash-img">
          <NavBarContainer />
          {/* </header> */}

          <SearchFormContainer />
        </div>

        <Footer />
      </div>
    )
  }
}

export default Splash;