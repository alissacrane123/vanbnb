import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
// import ListingMap from './listing_map';
import { fetchListings } from '../../actions/listing_actions';
import { updateFilter } from '../../actions/filter_actions';
import SearchBar from './search_bar';
import { receiveSearch } from '../../actions/search_actions';


const mapStateToProps = (state, ownProps) => {
  return {
    listings: Object.values(state.entities.listings),
    bounds: state.ui.filters.bounds,
    filters: state.ui.filters
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    fetchListings: () => dispatch(fetchListings()),
    updateFilter: (filter, value) => dispatch(updateFilter(filter, value)),
    receiveSearch: search => dispatch(receiveSearch(search))
    // getState: () => dispatch(getState())
  }
}


const SearchBarContainer = connect(mapStateToProps, mapDispatchToProps)(SearchBar);
export default withRouter(SearchBarContainer);