import React from 'react';

import ListingMap from './listing_map';
import ListingMapContainer from './map_container';
import ListingIndex from '../listing/listing_index';
import Loader from './loader';

class Search extends React.Component {
  constructor(props) {
    super(props);
    this.state = { listings: [], loaded: false }
    setTimeout(() => this.setState({ loaded: true }), 3000);
  }

  componentDidMount() {
    this.setState({ listings: this.props.listings })
  }

  render() {
    let { amenityLists, listings } = this.props;
    let component = this.state.loaded ? <ListingIndex listings={listings} amenityLists={amenityLists} /> : <Loader />
    return (
      <div className="search-container">

        <div className="search-listings">
          {/* <ListingIndex listings={listings} amenityLists={amenityLists}/> */}
          { component }
        </div>
        
        <div className="search-map">
          <ListingMapContainer />
        </div>
      </div>
    )
  }
}

export default Search;

// api key= AIzaSyAb1J8_aFITMmAJyOfXWARxFvU_WOdHAHk