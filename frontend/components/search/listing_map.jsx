import React from 'react';
import MarkerManager from '../../util/marker_manager';
import  {withRouter} from 'react-router-dom';
import queryString from 'query-string';


class ListingMap extends React.Component {
  constructor(props) {
    super(props);
    this.state;
    this.renderMap = this.renderMap.bind(this);
  }

  componentDidMount() {
    this.renderMap();
  }

  renderMap() {  
    let coords; 

    if (this.props.location.search) {
      coords = queryString.parse(this.props.location.search);
    } else {
      coords = this.props.search;
    }

    const mapOptions = {
      center: { lat: parseFloat(coords.lat), lng: parseFloat(coords.lng)}, // this is SF
      zoom: 12
    };
    this.map = new google.maps.Map(this.mapNode, mapOptions);
    this.registerListeners();
    this.MarkerManager = new MarkerManager(this.map);

    this.MarkerManager.updateMarkers(this.props.listings);

  }

  registerListeners() {
    this.map.addListener('idle', () => {
      let { north, south, east, west } = this.map.getBounds().toJSON();
      let bounds = {
        northEast: { lat: north, lng: east },
        southWest: { lat: south, lng: west }
      };

      this.props.updateFilter('bounds', bounds);
    })
  }

  componentWillUnmount() {
    google.maps.event.clearListeners(this.map, 'idle');
  }


  componentDidUpdate(prevProps) {
    this.MarkerManager.updateMarkers(this.props.listings);
    if (this.props.location.search != prevProps.location.search) {
      this.renderMap();
    }
  }


  render() {

    return (
      <div className="map-container" >
        <div id="map" ref={map => this.mapNode = map}>
          Map
        </div>
      </div>
    )
  }
}

export default withRouter(ListingMap);


