import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
// import ListingMap from './listing_map';
import { fetchListings } from '../../actions/listing_actions';
import { updateFilter } from '../../actions/filter_actions';
import SearchForm from './search_form';
import {receiveSearch} from '../../actions/search_actions';


const mapStateToProps = (state, ownProps) => {
  return {
    listings: Object.values(state.entities.listings),
    city: state.ui.filters.city,
    bounds: state.ui.filters.bounds,
    filters: state.ui.filters
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    fetchListings: () => dispatch(fetchListings()),
    updateFilter: (filter, value) => dispatch(updateFilter(filter, value)),
    receiveSearch: search => dispatch(receiveSearch(search))
    // getState: () => dispatch(getState())
  }
}


const SearchFormContainer = connect(mapStateToProps, mapDispatchToProps)(SearchForm);
export default withRouter(SearchFormContainer)