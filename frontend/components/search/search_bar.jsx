import React from 'react';
import { th } from 'date-fns/esm/locale';


class SearchBar extends React.Component {
  constructor(props) {
    super(props);
    this.state = { address: '' }
    this.handleUpdate = this.handleUpdate.bind(this);
    this.handleKeyPress = this.handleKeyPress.bind(this);
    this.clearInput = this.clearInput.bind(this);
  }

  clearInput() {
    // debugger
    this.setState({ address: '' })
  }

  handleUpdate(e) {
    this.setState({ address: e.currentTarget.value })
  }

  handleKeyPress(e) {
    if (!e || e.key === 'Enter') {
      if (e) e.preventDefault();
      let coord = new google.maps.Geocoder();
      coord.geocode({ "address": this.state.address }, (results, status) => {
        let lat, lng;
        if (status === 'OK') {
          
          lat = results[0].geometry.location.lat();
          lng = results[0].geometry.location.lng();
          this.props.receiveSearch({ lat, lng });
          this.props.history.push(`/listings?lat=${lat}&lng=${lng}`);
           
          this.props.fetchListings();
        } else {
          lat = 37.7558;
          lng = -122.435;
          this.props.receiveSearch({ lat, lng })
          this.props.history.push(`/listings?lat=${lat}&lng=${lng}`)
        }
      })
    }

  }

  componentDidMount() {
    this.props.fetchListings();
    const searchInput = document.getElementById("nav2-search");
    const autocomplete = new google.maps.places.Autocomplete(searchInput);
    google.maps.event.addDomListener(window, "load", autocomplete);
    let newAddress;
    autocomplete.addListener("place_changed", () => {
      if (!autocomplete.getPlace().formatted_address) {
        newAddress = autocomplete.getPlace().name;
        this.setState({ address: newAddress });
        this.handleKeyPress();
      } else {
        newAddress = autocomplete.getPlace().formatted_address;
        this.setState({ address: newAddress });
        this.handleKeyPress();
      }
    });
  }


  render() {
    // if (this.props.location.pathname !== '/' && this.props.location.pathname !== '/listings') {
    //   // debugger
    //   this.clearInput; }

    return (
      <div className="nav2-search-outer">
        <div className="nav2-search-inner">
          <img src="assets/search.png" className="search-icon"/>
          <input 
          onChange={this.handleUpdate} type="text" placeholder="Try &quot;San Francisco&quot;" id="nav2-search"
          onKeyPress={ this.handleKeyPress }  value={this.state.address} onClick={this.clearInput }/>
        </div>
      </div>
    )
  }
}

export default SearchBar;




