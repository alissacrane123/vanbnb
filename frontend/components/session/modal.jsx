import React from 'react';
import { connect } from 'react-redux';

import LoginFormContainer from './login_form_container';
import SignupFormContainer from './signup_form_container';
import CreateBookingContainer from '../booking/create_booking_container';

class Modal extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      showLogin: false,
      showSignup: false
    };
    this.demoLogin = this.demoLogin.bind(this);
  }

  demoLogin() {
    this.props.login({ email: 'demo@vanbnb.com', password:  'password' });
    this.props.closeModal();
  }

  render() {
    if (!this.props.modal) return null;

    if (this.props.modal === 'login'){
      return (
        <div className="modal-background" onClick={this.props.closeModal} >
          <div className="modal-child-login" onClick={e => e.stopPropagation()}>
            <div className="X-login">
              <img className="x" src="assets/x.png" onClick={this.props.closeModal}/>
            </div>
            <div className="modal-form-login">
              <div className="fb-google">
                <div className="session-button-login-fb">Log in below</div>
                <div id="demo" onClick={this.demoLogin} className="session-button-login-google">Demo Login</div>
                
              </div>
              <div className="login-line">
              _____________________________________________________________________________________
              </div>
              <LoginFormContainer />

            </div>
          </div>
        </div>
      )
    } else if (this.props.modal === 'signup') {
      return (
        <div className="modal-background" onClick={this.props.closeModal} >
          <div className="modal-child" onClick={e => e.stopPropagation()}>
            <div className="X-signup">
              <img className="x" src="assets/x.png" onClick={this.props.closeModal} />
            </div>
            <div className="modal-form">
              <SignupFormContainer />
            </div>
          </div>
        </div>
      )
    } else if (this.props.modal === 'booking') {
      return (
        <div className="modal-background-booking" onClick={this.props.closeModal} >
          <div className="modal-child-booking" onClick={e => e.stopPropagation()}>
            
            <div className="X2">
              X
            </div>
            <div className="modal-form-booking">
              <CreateBookingContainer />
            </div>
          </div>
        </div>       
      )
    } 
    // else if (this.props.modal === 'photo') {
    //   return (
    //     <div className="modal-background-booking" onClick={this.props.closeModal} >
    //       <div className="modal-child-booking" onClick={e => e.stopPropagation()}>

    //         <div className="modal-form-photo">
              
    //         </div>
    //       </div>
    //     </div>  
    //   )
    // }  
  }
}

export default Modal;