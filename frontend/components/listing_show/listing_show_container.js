import { connect } from 'react-redux';
// import { fetchReviews } from '../../actions/review_actions';
import { fetchListing } from '../../actions/listing_actions';
import { openModal, closeModal } from '../../actions/modal_actions';

import ListingShow from './listing_show';

const mapStateToProps = (state, ownProps) => {
  const listingId = parseInt(ownProps.match.params.listingId);
  const listing = state.entities.listings[listingId];
  const amenityList = state.entities.amenityLists
  
  return {
    listingId,
    listing,
    amenityList,
    reviews: Object.values(state.entities.reviews),
    loggedIn: Boolean(state.session.id)
  }
}

const mapDispatchToProps = dispatch=> ({
  fetchListing: id => dispatch(fetchListing(id)),
  openModal: modal => dispatch(openModal(modal)),
  // fetchReviews: () => dispatch(fetchReviews()),
  closeModal: () => dispatch(closeModal())
})

export default connect(mapStateToProps,mapDispatchToProps)(ListingShow);
