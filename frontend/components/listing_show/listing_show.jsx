import React from 'react';
import { Link } from 'react-router-dom';
import ReviewIndex from '../review/review_index';
import listingMap from '../search/listing_map';
import AmenityList from '../listing_show/amenity_list';
import { receiveReviews } from '../../actions/review_actions';

class listingShow extends React.Component {
  constructor(props) {
    super(props);
  }

  componentDidMount() {
    this.props.fetchListing(this.props.listingId);
    // this.props.fetchReviews();
  }

  render() {
    let { listing, listingId, amenityList, openModal, reviews, loggedIn } = this.props;
    // debugger
    if (listing === undefined || listingId === undefined) return null;
    if (amenityList === undefined) return null;
    if (amenityList === [] || amenityList === {}) return null;
    // if (reviews.length === 0) return null;

    let amenityListArr = Object.values(amenityList);
    let amenityObj;
    let amenities;

    if (amenityListArr[0] != undefined ) {

      amenityObj = amenityListArr[0];
      amenities = Object.keys(amenityObj).map(type => (
        <div key={type} className="amenity-box" >
          <div className="amenity-type">{type}</div>
          <div className="amenity-photo"></div>
        </div>
      ))
    } else {
      return null;
    }
    
    let photoUrl;
    if (listing.photoUrls.length >= 1) {
      photoUrl = listing.photoUrls[0];
    }

    let otherPhotos = listing.photoUrls.map(photo => (
      <div className="other-photo" key={photo}>
        <img src={photo} className="other-img" key={photo} />
      </div>
    ))

    reviews = reviews.filter(review => review.listing_id === listingId);

    return (
      <div className="show-body-container">
        <div className="show-body">
          
          <div className="show-photos-container">
            <div className="show-photo-name">
              <div className="show-name">{listing.name}</div>
            </div>
            <div className="show-photos">
              <img src={photoUrl} className="show-photo" />
            </div>
          </div>
      
          <div className="show-info-container">
        
            <div className="show-info-container2">
              <div className="show-info">
                <div className="guest-num">Room for {amenityObj.guest_num} guests</div>
              </div>
              <div className="show-description">
                <div className="description">{listing.description}</div>
              </div>
            </div>
            <div className="show-host">

            </div>
          </div>

          <div className="show-tour-container">
            <div className="show-amenities-title">
              <div className="amenities-title">Tour this van</div>
            </div>

            <div className="tour-container">
              {otherPhotos}
            </div>
          </div>

          <AmenityList amenityList={amenityList} />
        

          <div className="show-reviews-container">
            <div className="show-amenities-title">
              <div className="amenities-title">Reviews</div>
            </div>

            <div className="reviews-container">
              <ReviewIndex reviews={reviews} listing={listing} />
            </div>
          </div>

          <div className="index-footer-container3">
            <div className="index-footer-container4">
              <div className="index-personal3">LinkedIn</div>
              <div className="index-personal3">·</div>
              <div className="index-personal3">GitHub</div>
              <div className="index-personal3">·</div>
              <div className="index-personal3">Portfolio</div>
            </div>
          </div>
        
        </div>
        <div className="show-footer">
          <div className="show-footer-content">
            <div className="show-footer-left">
              <div className="footer-logo">
                <img className="logo3" src={'footer1.png'} />
              </div>
              <div className="footer-name">AVAILABLE IN SAN FRANCISCO</div>
            </div>

            <div className="show-footer-right">
              {/* <div className="footer-button"> */}
              <button onClick={loggedIn ? () => openModal('booking') : () => openModal('login')} className="show-footer-button">Request to Book</button>
              {/* </div> */}
              <div className="footer-price">${listing.price} / night</div>
            </div>
          </div>
        </div>
      </div>
    )
  }

}

export default listingShow;