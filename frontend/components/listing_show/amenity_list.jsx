import React from 'react';


class AmenityList extends React.Component {


  render() {
    let { amenityList } = this.props;
    let amenityListArr = Object.values(amenityList);
    let amenityObj;
    let amenities;

    if (amenityListArr[0] != undefined) {
      amenityObj = amenityListArr[0];
      let validAmenities = [];

      for (let key in amenityObj) {
        if (amenityObj[key] === true) {
          validAmenities.push(key);
        }
      }
      let amenityImages = [["/amenity1.jpeg", "Air Conditioning"], ["/amenity2.jpeg", "Heating"], ["/amenity3.jpeg", "TV"], ["/amenity4.jpeg", "Kitchen"]];
      let i;

      amenities = validAmenities.map(type => {
        if (type === "air_con") i = 0;       
        if (type === "heating") i = 1;
        if (type === "tv") i = 2;
        if (type === "kitchen") i = 3;
        
        return (
          <div key={type} className="amenity-box" >
            <div className="amenity-photo">
              <img className="amenity-img" src={amenityImages[i][0]} alt=""/>
            </div>
            <div className="amenity-type">
              {amenityImages[i][1]}
            </div>
          </div>
      )})
    } else {
      return null;
    }

    return (
      <div className="show-amenities-container">
        <div className="show-amenities-title">
          <div className="amenities-title">Amenities</div>
        </div>

        <div className="amenities-container">
          {amenities}
        </div>
        {/* <AmenityList listing={listing} amenityList={amenityList[0]} /> */}
      </div>
    )
  }
}


export default AmenityList;