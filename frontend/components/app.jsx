import React from 'react';
import { Route, Switch, withRouter } from 'react-router-dom';
import { AuthRoute, ProtectedRoute } from '../util/route_util';
import Footer from './footer';

import NavBarContainer2 from '../components/navbar/navbar2_container';
import ListingShowContainer from '../components/listing_show/listing_show_container';
import SearchContainer from '../components/search/search_container';
import CreateReviewContainer from '../components/review/create_review_container';
import Splash from '../components/splash/splash';
import Modal from '../components/session/modal_container';
import BookingIndexContainer from '../components/booking/booking_index_container';


const App = () => (
  <div>
    <Modal />
    {/* <header> */}
      {/* <NavBarContainer2 /> */}
    {/* </header> */}
    <div className="body-container">
      
    <Switch>
      <Route exact path="/" component={Splash} /> 
      <NavBarContainer2 />
    </Switch>
    <Switch>
      <Route exact path="/listings/:listingId" component={ListingShowContainer} />
      <Route path="/listings" component={SearchContainer} />
      <ProtectedRoute path="/review/:listingId" component={CreateReviewContainer} />
      {/* <Route exact path="/" component={Splash} />  */}
      <ProtectedRoute path="/bookings" component={BookingIndexContainer} />
    </Switch>
    </div>
    {/* <Footer /> */}
  </div>
);

export default withRouter(App);

// Route path="/signup" component={SignupFormContainer}


// class App extends React.Component {
//   constructor(props) {
//     super(props);

//     this.toggleCSS = this.toggleCSS.bind(this);
//   }

//   toggleCSS() {
//     $(".nav2-head").removeClass("nav2-head").addClass("nav2-head-splash")
//     $(".nav2-container").removeClass("nav2-container").addClass("nav2-container-splash")
//     $(".nav2-search-container").removeClass("nav2-search-container").addClass("nav2-search-container-splash")

//     // $(".nav2-head").toggle("nav2-head-splash")
//     // $(".nav2-container").toggle("nav2-container-splash")
//     // $(".nav2-search-container").toggle("nav2-search-container-splash")
//   }
//   toggleCSS2() {
//     $(".nav2-head-splash").removeClass("nav2-head-splash").addClass("nav2-head")
//     $(".nav2-container-splash").removeClass("nav2-container-splash").addClass("nav2-container")
//     $(".nav2-search-container-splash").removeClass("nav2-search-container-splash").addClass("nav2-search-container")

//     // $(".nav2-head").toggle("nav2-head-splash")
//     // $(".nav2-container").toggle("nav2-container-splash")
//     // $(".nav2-search-container").toggle("nav2-search-container-splash")
//   }

//   render() {
//     if (this.props.location.pathname === '/' &&
//       !document.getElementById("sp").classList.contains("splash")) {
//       this.toggleCSS();
//     } else {
//       this.toggleCSS2();
//     }

//     return (
//       <div>
//         <Modal />
//         {/* <header> */}
//         <NavBarContainer2 />
//         {/* </header> */}
//         <div className="body-container">
//           <Switch>
//             <Route exact path="/listings/:listingId" component={ListingShowContainer} />
//             <Route path="/listings" component={SearchContainer} />
//             <Route path="/review/:listingId" component={CreateReviewContainer} />
//             <Route exact path="/" component={Splash} />
//             <Route path="/bookings" component={BookingIndexContainer} />
//           </Switch>
//         </div>
//         {/* <Footer /> */}
//       </div>
//     )
//   }
// }