import React from 'react';
import { Link } from 'react-router-dom';
import SearchBarContainer from '../search/search_bar_container';
import { withRouter } from 'react-router-dom';

class NavBar2 extends React.Component {
  constructor(props) {
    super(props);
    this.handleClick = this.handleClick.bind(this);
    this.handleDrop = this.handleDrop.bind(this);
    this.handleDrop2 = this.handleDrop2.bind(this);
    // this.toggleCSS = this.toggleCSS.bind(this);
  }


  handleClick() {
    this.props.history.push('/bookings')
  }


  handleDrop() {
    $(".drop-content").toggle("show")
  }

  handleDrop2() {
    $(".drop-content2").toggle("show")
  }


  render() {
    
    let {openModal, updateFilter, filters, location} = this.props;
    // debugger


    let navLogo, splashClass;
    if (this.props.location.pathname === '/') {
      navLogo = (
        <div className="nav-logo">
          <img className="logo" src={'splash-logo.png'} />
        </div>
      )
      splashClass = '-splash';

    } else {
      navLogo = (
        <div className="nav2-logo">
          <Link to={"/"} >
            {navLogo}
            <img className="logo2" src={'other-logo2.png'} />
          </Link>
        </div>
      )
      splashClass = '';
    }
    
    const loggedInButtons = (
      <div id="sp" className="nav2-buttons-container" >
        <div className="dropdown">
          <button className="prof-button" onClick={this.handleDrop}>
            <div className={`logo-cont${splashClass}`}>
              <img className={`prof-logo${splashClass}`} src={'prof.png'} />
            </div>
          </button>
          <div id="dropdown" className="drop-content">
            <button className="drop-button" onClick={this.props.logout}>Logout</button>
          </div>
        </div>
        {/* <button className={`"nav2-button"`} onClick={this.props.logout}>Logout</button> */}
        
        <div className="dropdown2">
          <button className={`nav2-button${splashClass}`} onClick={this.handleDrop2}>Help</button>
          <div id="dropdown" className="drop-content2">
            hi
          </div>
        </div>

        <button className={`nav2-button${splashClass}`} onClick={this.handleClick}>Trips</button>
      </div>
    );


    const loggedOutButtons = (
      <div className="nav2-buttons-container">
        <button className={`nav2-button${splashClass}`} onClick={() => openModal('login')}>Log in</button>
        <button className={`nav2-button${splashClass}`} onClick={() => openModal('signup')}>Sign up</button>
        <div className="dropdown2">
          <button className={`nav2-button${splashClass}`} onClick={this.handleDrop2}>Help</button>
          <div id="dropdown" className="drop-content2">
            hi
          </div>
        </div>

      </div>
    );

    
    return (
      // <header className="nav2-head">
      <header className={`nav2-head${splashClass}`}>
        <div className={`nav2-container${splashClass}`}>

          <div className={`nav2-search-container${splashClass}`}>
            {/* <div className="nav2-logo"> */}
              {/* <Link to={"/"} > */}
                { navLogo }
              {/* <img className="logo2" src={'other-logo2.png'} /> */}
              {/* </Link> */}
            {/* </div>                */}
            { location.pathname === '/' ? <div></div> :<SearchBarContainer/> }              
          </div>
          {this.props.currentUser ? loggedInButtons : loggedOutButtons}
        </div>
      </header>
    )
    } 
  }
// }

export default withRouter(NavBar2);



  // toggleCSS() {
  //   debugger
  //   if (this.props.location.pathname === '/' &&
  //     !($("#sp").hasClass("splash"))) {
  //     $(".nav2-head").removeClass("nav2-head").addClass("nav2-head-splash");
  //     $(".nav2-container").removeClass("nav2-container").addClass("nav2-container-splash");
  //     $(".nav2-search-container").removeClass("nav2-search-container").addClass("nav2-search-container-splash");
  //   }
  // }
    // $(".nav2-head").toggle("nav2-head-splash")
    // $(".nav2-container").toggle("nav2-container-splash")
    // $(".nav2-search-container").toggle("nav2-search-container-splash")
  // }
