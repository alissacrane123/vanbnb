
import { fetchListings, fetchListing } from '../../actions/listing_actions';
import { fetchAmenityList } from '../../actions/amenity_actions';
import { updateFilter } from '../../actions/filter_actions';

import { receiveSearch } from '../../actions/search_actions';

const mapStateToProps = (state, ownProps) => {
  // 
  return {
    listings: Object.values(state.entities.listings),
    search: state.ui.search,
    amenityLists: state.entities.amenityLists,
    filters: state.ui.filters,
    queryString: ownProps.location.search

  }
}

const mapDispatchToProps = dispatch => {
  return {
    fetchListings: filters => dispatch(fetchListings(filters)),
    updateFilter: (filter, value) => dispatch(updateFilter(filter, value)),
    receiveSearch: search => dispatch(receiveSearch(search)),
    fetchAmenityList: id =>  dispatch(fetchAmenityList(id)),
    fetchListing: id => dispatch(fetchListing(id)),
    
  }
}

// const ListingIndexContainer = connect(mapStateToProps, mapDispatchToProps)(ListingIndex);
// export default withRouter(ListingIndexContainer);