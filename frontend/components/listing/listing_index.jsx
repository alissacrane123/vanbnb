import React from 'react';
import ListingIndexItem from './listing_index_item';


class ListingIndex extends React.Component {
  constructor(props) {
    super(props);
  }
  
  render() {
    let { amenityLists } = this.props; 

    const listings = this.props.listings.map(listing => {
      let amenityList = amenityLists[listing.id]
      
      return (<ListingIndexItem amenityList={amenityList} key={listing.id} listing={listing} />)
    });
        
    const count = listings.length;
    // debugger
    return (
      <div className="listings-index-container">

        <div className="listings-index-header">
          <div className="index-logo-container">
            <img className="index-logo" src={'index-logo.png'} />
          </div>
          <div className="index-aside">
            <div className="aside-text">
              Voted best van rental site 100 years in a row!
            </div>
          </div>
        </div>

        <div className="listings-container">

          <div className="listings-header-container">
            <div className="listings-header">
              <p>There are {count} available vans in your area.</p>
            </div>
          </div>

          <ul className="listings-ul">
            {/* <div className="listings-li"> */}
              { listings }
            {/* </div> */}
          </ul>

          <div className="index-footer-container">
            <div className="index-footer-container2">
              <div className="index-personal">LinkedIn</div>
              <div className="index-personal">·</div>
              <div className="index-personal">GitHub</div>
              <div className="index-personal">·</div>
              <div className="index-personal">Portfolio</div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default ListingIndex;

