import React from 'react';
// import { Link } from 'react-router-dom';
import { withRouter } from 'react-router-dom';
import ListingShowContainer from '../listing_show/listing_show_container';

class ListingIndexItem extends React.Component {
  constructor(props) {
    super(props);
    this.handleClick = this.handleClick.bind(this);
  }

  handleClick() {
    let { listing } = this.props;
    const listingId = listing.id;

    this.props.history.push(`/listings/${listingId}`)
  }

  render() {
    let { listing, amenityList } = this.props;
    let photoUrl;
    if (listing.photoUrls.length >= 1) {
      photoUrl = listing.photoUrls[0];
    }
    if (!amenityList) return null;
    let amenityArr = Object.keys(amenityList).filter(amenity => (
      amenityList[amenity] === true
    ));
    let amenityArr2 = [];

    amenityArr.map(amenity => {
      if (amenity === 'air_con') {
        amenityArr2.push( 'Air Conditioning');
      } else {
        amenityArr2.push(amenity.charAt(0).toUpperCase() + amenity.slice(1));
      }
    })

    let amenities = amenityArr2.join(' · ');


    return (
    <div className="list-item-container">
      <ul className="list-item-ul">
        <div className="list-item-content">
          <div className="list-item-link" onClick={this.handleClick} >

            <div className="list-item-photo">
               <img src={photoUrl} className="index-photo"/>
            </div>
            <div className="list-item-info">

              <ul className="item-info-ul">
                <div className="listing-sf">SAN FRANCISCO</div>
                <div className="item-info">
                  <div className="item-name">
                    {listing.name}
                  </div>
                  <div className="item-guests">
                      <div className="index-guests">Room for {amenityList.guest_num}</div>
                  </div>
                  <div className="item-amenities">
                    {amenities}
                  </div>
                </div>
              </ul>
              <div className="list-item-price-container">
                <div className="heart">
                  {/* heart */}
                </div>
                <div className="list-item-price">
                  ${listing.price}/day
                </div>
              </div>
            </div>

          </div>
        </div>
      </ul>

    </div>
  )}
}

export default withRouter(ListingIndexItem);