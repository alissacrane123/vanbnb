
export const fetchListings = (data) => {
  // debugger
  return $.ajax({
    method: 'GET',
    url: '/api/listings',
    data
  })
}

export const fetchListing = (id) => {
  
  return $.ajax({
    method: 'GET',
    url: `/api/listings/${id}`
  })
}