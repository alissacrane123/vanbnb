
export const createReview = review => {
  return $.ajax({
    method: 'POST',
    url: '/api/reviews',
    data: { review } 
  })
}

// export const fetchListingReviews = listing_id => {
//   return $.ajax({
//     method: 'GET',
//     url: '/api/reviews',
//     data: listing_id 
//   })
// }

export const fetchReviews = () => {
  return $.ajax({
    method: 'GET',
    url: 'api/reviews'
  })
}