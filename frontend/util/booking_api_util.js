export const createBooking = booking =>  {
  
  return $.ajax({
    method: 'POST',
    url: 'api/bookings',
    data: { booking }
  })
}

export const fetchBooking = bookingId => (
  $.ajax({
    method: 'GET', 
    url: `api/bookings/${bookingId}`
  })
)

export const fetchBookings = () => {
  return $.ajax({
    method: 'GET',
    url: 'api/bookings'
  })
}