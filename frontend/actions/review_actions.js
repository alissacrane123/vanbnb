import * as ReviewAPIUtil from '../util/review_api_util';

export const RECEIVE_REVIEW = 'RECEIVE_REVIEW';
export const RECEIVE_REVIEWS = 'RECEIVE_REVIEWS';

export const receiveReview = review => {
  return {
    type: RECEIVE_REVIEW,
    review
  }
}

export const receiveReviews = reviews => {
  return {
    type: RECEIVE_REVIEWS,
    reviews
  }
}

export const createReview = review => dispatch => (
  ReviewAPIUtil.createReview(review).then(review => dispatch(receiveReview(review)))
)

export const fetchListingReviews = listing_id => dispatch => (
  ReviewAPIUtil.fetchListingReviews(listing_id).then(reviews => dispatch(receiveReviews(reviews)))
)

export const fetchReviews = () => dispatch => (
  ReviewAPIUtil.fetchReviews().then(reviews => dispatch(receiveReviews(reviews)))
)