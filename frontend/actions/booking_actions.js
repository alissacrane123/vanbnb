import * as BookingAPIUtil from '../util/booking_api_util';

export const RECEIVE_BOOKING = 'RECEIVE_BOOKING';
export const RECEIVE_BOOKINGS = 'RECEIVE_BOOKINGS'

export const receiveBooking = (booking, CRUDtype) => {

  return {
    type: RECEIVE_BOOKING,
    booking,
    CRUDtype
  }
}

export const receiveBookings = payload => {
  // debugger
  return {
    type: RECEIVE_BOOKINGS,
    payload
  }
}

export const fetchBooking = bookingId => dispatch => (
  BookingAPIUtil.fetchBooking(bookingId).then(payload => dispatch(receiveBooking(payload)))
)

export const createBooking = booking => dispatch => (
  BookingAPIUtil.createBooking(booking).then(payload => dispatch(receiveBooking(payload, 'create')))
)

export const fetchBookings = () => dispatch => (
  BookingAPIUtil.fetchBookings().then(payload => dispatch(receiveBookings(payload)))
)