import * as AmenityListAPIUtil from '../util/amenity_api_util';

export const RECEIVE_AMENITY_LISTS = 'RECEIVE_AMENITY_LISTS';
export const RECEIVE_AMENITY_LIST = 'RECEIVE_AMENITY_LIST'

export const receiveAmenityLists = amenityLists => ({
  type: RECEIVE_AMENITY_LISTS,
  amenityLists
});

export const receiveAmenityList = ({ amenityList }) => ({
  type: RECEIVE_AMENITY_LIST,
  amenityList
})


export const fetchAmenityLists = () => dispatch => {
  return (
    AmenityListAPIUtil.fetchAmenityLists().then(amenityLists => dispatch(receiveAmenityLists(amenityLists)))
  )
};

export const fetchAmenityList = (listing) => dispatch => {
  return (
    AmenityListAPIUtil.fetchAmenityList(listing).then(payload => dispatch(receiveAmenityList(payload)))
  )
};