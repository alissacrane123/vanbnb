import { RECEIVE_LISTINGS, RECEIVE_LISTING } from '../actions/listing_actions';
import merge from 'lodash';
import { RECEIVE_BOOKINGS } from '../actions/booking_actions'

const listingsReducer = (state = {}, action) => {
  Object.freeze(state);
  switch (action.type) {  
    case RECEIVE_LISTINGS:
      if (!action.payload.listings) return {};
        return action.payload.listings;
    case RECEIVE_LISTING:
      const newState = { [action.payload.listing.id]: action.payload.listing}
      return Object.assign({}, newState);
    case RECEIVE_BOOKINGS:
      // debugger
      if (!action.payload.listings) return state;
      return action.payload.listings 
    default:
      return state;
  }
}

export default listingsReducer;