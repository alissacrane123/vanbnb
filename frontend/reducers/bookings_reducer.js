import { RECEIVE_BOOKING, RECEIVE_BOOKINGS } from '../actions/booking_actions';
import { RECEIVE_LISTING } from '../actions/listing_actions';


const bookingsReducer = (state = {}, action) => {
  Object.freeze(state);
  let newState = Object.assign({}, state)
  switch (action.type) {
    case RECEIVE_BOOKING:
      return Object.assign(newState, action.booking);
    case RECEIVE_BOOKINGS:
      if (!action.payload.bookings) return newState;

      
      return action.payload.bookings// all bookings
    case RECEIVE_LISTING:
      if (!action.payload.bookings) return newState;

      return action.payload.bookings 
    default:
      return state;
  }
}

export default bookingsReducer;