import { RECEIVE_REVIEW, RECEIVE_REVIEWS } from '../actions/review_actions';
import { RECEIVE_LISTING } from  '../actions/listing_actions';

const ReviewsReducer = (state = {}, action) => {
  Object.freeze(state);
  let newState = Object.assign({}, state);
  switch (action.type) {
    case RECEIVE_REVIEW:
      return Object.assign(newState, action.review);
    case RECEIVE_REVIEWS:
      return action.reviews;
    case RECEIVE_LISTING:
      if (action.payload.reviews) {
        return action.payload.reviews;
      } else {
        return state;
      }
    default:
      return state;
  }
}

export default ReviewsReducer;