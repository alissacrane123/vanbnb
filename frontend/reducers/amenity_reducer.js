import { RECEIVE_LISTING, RECEIVE_LISTINGS } from '../actions/listing_actions';

const amenityReducer = (state = {}, action) => {
  Object.freeze(state);
  switch (action.type) {
    case RECEIVE_LISTING:
      // 
      return { [action.payload.amenityList.id]: action.payload.amenityList }
    // case RECEIVE_AMENITY_LIST:
    //   const newState = { [action.amenityList.id]: action.amenityList }
    //   return merge({}, state, newState);
    case RECEIVE_LISTINGS:
      // 
      if (!action.payload.amenityLists) return state;
      return action.payload.amenityLists;
    default:
      return state;
  }
}

export default amenityReducer;