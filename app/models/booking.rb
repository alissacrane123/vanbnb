class Booking < ApplicationRecord
  validates :check_in, :check_out, presence: true

  belongs_to :listing, class_name: :Listing, foreign_key: :listing_id 
  belongs_to :guest, class_name: :User, foreign_key: :guest_id 
  belongs_to :owner, class_name: :User, foreign_key: :owner_id

  
end
