class Review < ApplicationRecord
  validates :rating, inclusion: { in: (1..5) }

  belongs_to :author, class_name: :User, foreign_key: :author_id
  belongs_to :listing, class_name: :Listing, foreign_key: :listing_id
  
end
