class AmenityList < ApplicationRecord
  validates :listing_id, presence: true

  belongs_to :listing, class_name: :Listing, foreign_key: :listing_id 

end
