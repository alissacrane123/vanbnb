class Api::ReviewsController < ApplicationController
  # before_action :require_logged_in

  def index 
    # debugger
    @reviews = Review.all 
  end

  def create
    @review = Review.new(review_params);
    @review.author_id = current_user.id 

    if @review.save
      render :show
    else 
      render json: @review.errors.full_messages
    end
  end

  private

  def review_params 
    params.require(:review).permit(:body, :listing_id, :rating)
  end
end
