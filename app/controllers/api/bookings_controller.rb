class Api::BookingsController < ApplicationController

  def index 
    # debugger
    @bookings = current_user.bookings 
  end

  def create
    @booking = Booking.new(booking_params)
    @booking.guest_id = current_user.id 
    listing = Listing.find(params[:booking][:listing_id])
    
    if @booking.save
      @bookings = current_user.bookings 
      render :index 
    else
      render json: @booking.errors.full_messages, status: 422
      # render json: ["The dates you selected are not available! Please choose again."], status: 422
    end
  end

  def update
  end

  def destroy
  end

  private

  def booking_params 
    params.require(:booking).permit(:listing_id, :num_guest, :check_in, :check_out, :owner_id)
  end
end
