@amenity_lists.each do |amenity_list|
  json.set! amenity_list.id do 
    json.partial! 'amenity_list', amenity_list: amenity_list
  end
end 