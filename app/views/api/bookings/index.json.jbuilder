@bookings.each do |booking|
  json.bookings do 
    json.set! booking.id do 
      json.partial! 'booking', booking: booking
    end 
  end

  json.listings do 
    json.set! booking.listing.id do 
      json.partial! "api/listings/listing", listing: booking.listing
      json.photoUrls booking.listing.photos.map { |file| url_for(file)}
    end
  end
end 


