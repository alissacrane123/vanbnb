json.set! "listings" do
  @listings.each do |listing|
    json.set! listing.id do 
      json.partial! 'listing', listing: listing
      json.photoUrls listing.photos.map { |file| url_for(file)}
    end
  end 
end 

json.set! "amenityLists" do 
  @listings.each do |listing|
    json.set! listing.id do 
      json.partial! "api/amenity_lists/amenity_list", amenity_list: listing.amenity_list
    end 
  end 
end 

