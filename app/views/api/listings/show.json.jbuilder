
json.set! "listing" do 
  json.partial! 'listing', listing: @listing
  json.photoUrls @listing.photos.map { |file| url_for(file)}
end


json.set! "amenityList" do
  json.partial! "api/amenity_lists/amenity_list", amenity_list: @listing.amenity_list 
  
end

json.set! "bookings" do 
  @listing.bookings.each do |booking|

    if current_user && booking.guest_id == current_user.id 
      json.set! booking.id do
        json.partial! "api/bookings/booking", booking: booking
      end 
    end
  end 
end 

json.set! "reviews" do 

  @listing.reviews.each do |review|
    json.set! review.id do 
      json.partial! "api/reviews/review", review: review 
    end 
  end
end